///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file node.cpp
/// @version 1.0
///
/// A generic Node class.  May be used as a base class for a Doubly Linked List
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   8 April 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>

#include "node.hpp"

using namespace std;

void Node::dump() const{
   cout << "Node: prev=[" << prev << "]  current=[" << this << "]  next=[" << next << "]" << endl; 
}

// This sorts based on the Node's address (because Node doesn't hold any
// data).  However, classes like Cat can override this operator and implement
// their own lexigraphic test for sorting.

bool Node::operator>(const Node& rightSide) {
	// this is the leftSide of the operator, so compare:
	// leftSide > rightSide

	if( this > &rightSide )
		return true;
	return false;
}
