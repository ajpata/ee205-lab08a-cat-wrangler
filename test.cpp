///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file test.cpp
/// @version 2.0
///
/// Unit test for this lab
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   8 April 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>

#include "cat.hpp"
#include "list.hpp"

using namespace std;


int main() {
	cout << "Cat Wrangler Unit Tests" << endl;

	Cat::initNames();

	DoubleLinkedList list = DoubleLinkedList();

	assert( list.validate() );

   // Test push_front
   cout << endl << "push_front: add 10 cats" << endl;
	for( int i = 0 ; i < 10 ; i++ ) {
		list.push_front( Cat::makeCat() );
		assert( list.validate() );
	}
   list.dump();
   cout << "Size: " << list.size() << endl << endl;

   cout << "push_front: add 10 more to front" << endl;
	for( int i = 0 ; i < 10 ; i++ ) {
		list.push_front( Cat::makeCat() );
		assert( list.validate() );
	}
   list.dump();
   cout << "Size: " << list.size() << endl << endl;

   // Test pop_front
   cout << "pop_front: remove first 10" << endl;
	for( int i = 0 ; i < 10 ; i++ ) {
		list.pop_front();
		assert( list.validate() );
	}
   list.dump();
   cout << "Size: " << list.size() << endl << endl;
   
   cout <<"pop_front: remove remaining" << endl;
	for( int i = 0 ; i < 10 ; i++ ) {
		list.pop_front();
		assert( list.validate() );
	}
   list.dump();
   cout << "Size: " << list.size() << endl << endl;
   
   // Test push_back
   cout << "push_back: add 10 cats" << endl;
	for( int i = 0 ; i < 10 ; i++ ) {
		list.push_back( Cat::makeCat() );
		assert( list.validate() );
	}
   list.dump();
   cout << "Size: " << list.size() << endl << endl;
   
   cout << "push_back: add 10 more to back" << endl;
	for( int i = 0 ; i < 10 ; i++ ) {
		list.push_back( Cat::makeCat() );
		assert( list.validate() );
	}
   list.dump();
   cout << "Size: " << list.size() << endl << endl;
   
   // Test pop_back
   cout << "pop_back: remove last 10" << endl;
	for( int i = 0 ; i < 10 ; i++ ) {
		list.pop_back();
		assert( list.validate() );
	}
   list.dump();
   cout << "Size: " << list.size() << endl << endl;

   cout << "pop_back: remove remaining" << endl;
	for( int i = 0 ; i < 10 ; i++ ) {
		list.pop_back();
		assert( list.validate() );
	}
   list.dump();
   cout << "Size: " << list.size() << endl << endl;





	Cat* cat1 = Cat::makeCat();
	Cat* cat2 = Cat::makeCat();
	Cat* cat3 = Cat::makeCat();
	Cat* cat4 = Cat::makeCat();
	Cat* cat5 = Cat::makeCat();

	// Test insert_after
	cout << endl << "Test insert_after" << endl;
   cout << endl << "Cats used for insert_after:" << endl;
   cout << "Cat1 = [" << (void*)cat1 << "]" << endl;
   cout << "Cat2 = [" << (void*)cat2 << "]" << endl;
   cout << "Cat3 = [" << (void*)cat3 << "]" << endl;
   cout << "Cat4 = [" << (void*)cat4 << "]" << endl;
   cout << "Cat5 = [" << (void*)cat5 << "]" << endl;


	assert( list.validate() );
	assert( list.get_first() == nullptr );
	assert( list.get_last() == nullptr );

	list.insert_after( nullptr, cat1 ); //1
	assert( list.isIn( cat1 ));
	assert( !list.isIn( cat2 ));
	assert( list.validate() );
   cout << endl << "Size: " << list.size() << endl;
   list.dump();      
	
   list.insert_after( cat1, cat2 );    //1 2
	assert( list.isIn( cat2 ));
	assert( list.validate() );
   cout << endl << "Size: " << list.size() << endl;
   list.dump();      

	list.insert_after( cat1, cat3 );    //1 3 2
	assert( list.isIn( cat3 ));
	assert( list.validate() );
   cout << endl << "Size: " << list.size() << endl;
   list.dump();      

	assert( list.validate() );
	list.insert_after( cat3, cat4 );    //1 3 4 2
	assert( list.isIn( cat4 ));
	assert( list.validate() );
   cout << endl << "Size: " << list.size() << endl;
   list.dump();      

	assert( list.validate() );
	list.insert_after( cat2, cat5 );    //1 3 4 2 5
	assert( list.isIn( cat5 ));
	assert( list.validate() );
   cout << endl << "Size: " << list.size() << endl;
   list.dump();      

	list.pop_back();
	list.pop_back();
	list.pop_back();
	list.pop_back();
	list.pop_back();
   assert(list.empty());

	// Test insert_before
	cout << endl << "Test insert_before" << endl;
   cout << endl << "Cats used for insert_before:" << endl;
   cout << "Cat1 = [" << (void*)cat1 << "]" << endl;
   cout << "Cat2 = [" << (void*)cat2 << "]" << endl;
   cout << "Cat3 = [" << (void*)cat3 << "]" << endl;
   cout << "Cat4 = [" << (void*)cat4 << "]" << endl;
   cout << "Cat5 = [" << (void*)cat5 << "]" << endl;

   assert( list.validate() );
   assert( list.get_first() == nullptr );
   assert( list.get_last() == nullptr );

   list.insert_before( nullptr, cat1 ); //1
   assert( list.isIn( cat1 ));
   assert( !list.isIn( cat2 ));
   assert( list.validate() );
   cout << endl << "Size: " << list.size() << endl;
   list.dump();

   list.insert_before( cat1, cat2 );    //2 1
   assert( list.isIn( cat2 ));
   assert( list.validate() );
   cout << endl << "Size: " << list.size() << endl;
   list.dump();

   list.insert_before( cat1, cat3 );    //2 3 1
   assert( list.isIn( cat3 ));
   assert( list.validate() );
   cout << endl << "Size: " << list.size() << endl;
   list.dump();

   list.insert_before( cat3, cat4 );    //2 4 3 1
   assert( list.isIn( cat4 ));
   assert( list.validate() );
   cout << endl << "Size: " << list.size() << endl;
   list.dump();

   list.insert_before( cat2, cat5 );    //5 2 4 3 1
   assert( list.isIn( cat5 ));
   assert( list.validate() );
   cout << endl << "Size: " << list.size() << endl;
   list.dump();

   list.pop_back();
   list.pop_back();
   list.pop_back();
   list.pop_back();
   list.pop_back();
   assert(list.empty());


	// Test swap function
	cout << endl << "Test swap" << endl;
   cout << endl << "Cats used for swap:" << endl;
   cout << "Cat1 = [" << (void*)cat1 << "]" << endl;
   cout << "Cat2 = [" << (void*)cat2 << "]" << endl;
   cout << "Cat3 = [" << (void*)cat3 << "]" << endl;
   cout << "Cat4 = [" << (void*)cat4 << "]" << endl;
   cout << "Cat5 = [" << (void*)cat5 << "]" << endl;

	list.push_front( cat1 );
	list.swap( cat1, cat1 );     // 1 case 1
	assert( list.validate() );

   list.push_back(cat2);        //1 2
   list.swap( cat1, cat2 );     //2 1 case 2 
   assert(list.validate());
   list.swap( cat1, cat2 );     //1 2 case 3
   assert(list.validate());

   list.push_back(cat3);        //1 2 3
   list.push_back(cat4);        //1 2 3 4
   list.push_back(cat5);        //1 2 3 4 5
   list.swap( cat1, cat5 );     //5 2 3 4 1 case 4
   assert(list.validate());
   list.swap( cat5, cat2 );     //2 5 3 4 1 case 5
   assert(list.validate());
   list.swap( cat4, cat1 );     //2 5 3 1 4 case 6
   assert(list.validate());
   list.swap( cat5, cat3 );     //2 3 5 1 4 case 7
   assert(list.validate());
   list.swap( cat3, cat2 );     //3 2 5 1 4 case 8
   assert(list.validate());
   list.swap( cat4, cat1 );     //3 2 5 4 1 case 9
   assert(list.validate());
   list.swap( cat4, cat5 );     //3 2 4 5 1 case 10
   assert(list.validate());
   list.swap( cat3, cat4 );     //4 2 3 5 1 case 11
   assert(list.validate());
   list.swap( cat5, cat4 );     //5 2 3 4 1 case 12
   assert(list.validate());
   list.swap( cat1, cat3 );     //5 2 1 4 3 case 13
   assert(list.validate());
   list.swap( cat2, cat3 );     //5 3 1 4 2 case 14
   assert(list.validate());
   list.swap( cat3, cat4 );     //5 4 1 3 2 case 15
   assert(list.validate());
   
   cout << endl << "Final comparison for swap" << endl;
   cout << "cat5: [" << (void*)cat5 << "] ->";
   cout << "cat4: [" << (void*)cat4 << "] ->";
   cout << "cat1: [" << (void*)cat1 << "] ->";
   cout << "cat3: [" << (void*)cat3 << "] ->";
   cout << "cat2: [" << (void*)cat2 << "] " << endl << endl;
   list.dump();


	cout << endl << "Test Insertion Sort" << endl;
   cout << endl << "Cats used for insertion sort:" << endl;
   cout << "Cat1 = [" << cat1->getName() << "]" << endl;
   cout << "Cat2 = [" << cat2->getName() << "]" << endl;
   cout << "Cat3 = [" << cat3->getName() << "]" << endl;
   cout << "Cat4 = [" << cat4->getName() << "]" << endl;
   cout << "Cat5 = [" << cat5->getName() << "]" << endl;
   
   cout << endl << "Current list:" << endl;
   //list.dump();
   list.insertionSort();
   cout << endl << "After sort:" << endl;
   assert(list.isSorted());
   list.dump();

   list.pop_front();
   list.pop_front();
   list.pop_front();
   list.pop_front();
   list.pop_front();
   assert(list.empty());

   cout << endl << "Professor's Original Insertion Test" << endl;

   // Empty list
	list.insertionSort();
	assert( list.isSorted() );

	// One item in list
	list.push_front( cat1 );
	assert( list.isSorted() );
	list.insertionSort();
   list.dump();
	assert( list.isSorted() );

	// Two items in list
	list.push_front( cat2 );
   list.insertionSort();
	list.dump();
	assert( list.isSorted() );

	// Three items in list
	list.push_front( cat3 );
	list.insertionSort();
	list.dump();
	assert( list.isSorted() );

	// Four items in list
	list.push_front( cat4 );
	list.insertionSort();
	list.dump();
	assert( list.isSorted() );

	// Five items in list
	list.push_front( cat5 );
	list.insertionSort();
	list.dump();
	assert( list.isSorted() );
   
   list.pop_front();
   list.pop_front();
   list.pop_front();
   list.pop_front();
   list.pop_front();
   assert(list.empty());


	cout << endl << "Final insertion test: 100 iterations of 1000 cats" << endl;
   cout << "Iteration finished: ";

   for( int i = 0 ; i < 100 ; i++ ) {  // Outer loop
		DoubleLinkedList dll = DoubleLinkedList();
		for( int j = 0 ; j < 1000 ; j++ ) {
			dll.push_front( Cat::makeCat() );
		}
		assert( dll.validate() );
		assert( dll.size() == 1000 );
		assert( !dll.isSorted() );

		dll.insertionSort();
		assert( dll.isSorted() );

		for( int j = 0 ; j < 1000 ; j++ ) {
			delete (Cat*) dll.pop_front();
		}
      assert(dll.empty());
      
      cout << i + 1 << " ";
		cout << std::flush;
	}

   cout << endl << "All iterations finished" << endl;
   cout << endl << "Finished all Cat Wrangler Unit Tests" << endl;
}

