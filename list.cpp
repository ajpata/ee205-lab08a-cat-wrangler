///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   8 April 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>

#include "list.hpp"
#include "cat.hpp"

using namespace std;

const bool DoubleLinkedList::empty() const{
   return count == 0;
}





Node* DoubleLinkedList::get_first() const {
   return head;
}

Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   return currentNode->next;
}

Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
   return currentNode->prev;
}

Node* DoubleLinkedList::get_last() const {
   return tail;
}





void DoubleLinkedList::push_front( Node* newNode ) {
   if(newNode == nullptr)
      return;

   if(head != nullptr) {
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;
   } else {
      newNode->prev = nullptr;
      newNode->next = nullptr;
      head = newNode;
      tail = newNode;
   }
  
   count++;
}

Node* DoubleLinkedList::pop_front() {
   if( empty() ){
      return nullptr;
   } if (count == 1) {
      head = nullptr;
      tail = nullptr;
      count--;
      return nullptr;
   } else {
      Node* oldHead = head;
      head = oldHead->next;
      head->prev = nullptr;
      count--;
      return oldHead;
   }
}

void DoubleLinkedList::push_back(Node* newNode) {
   if(newNode == nullptr)
      return;

   if(tail != nullptr) {
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail = newNode;
   } else {
      newNode->prev = nullptr;
      newNode->next = nullptr;
      head = newNode;
      tail = newNode;
   }

   count++;
}

Node* DoubleLinkedList::pop_back() {
   if( empty() ){
      return nullptr;
   } if (count == 1) {
      head = nullptr;
      tail = nullptr;
      count--;
      return nullptr;
   } else {
      Node* oldTail = tail;
      tail = oldTail->prev;
      tail->next = nullptr;
      count--;
      return oldTail;
   }
}





bool DoubleLinkedList::validate() {
   if(head == nullptr) {
      assert(tail == nullptr);
      assert(count == 0);
   } else {
      assert(tail != nullptr);
      assert( count != 0);
   }

   if(tail == nullptr) {
      assert(head == nullptr);
      assert(count == 0);
   } else {
      assert(head != nullptr);
      assert(count!= 0);
   }

   if(head == tail && head != nullptr) {
      assert(count == 1);
   }

   unsigned int fCount = 0;
   Node* currentNode = head;
   while(currentNode != nullptr) {
      fCount++;
      if(currentNode->next != nullptr) {
         assert(currentNode->next->prev == currentNode);
      }
      currentNode = currentNode->next;
   }
   assert(count == fCount);

   unsigned int bCount = 0;
   currentNode = tail;
   while(currentNode != nullptr) {
      bCount++;
      if(currentNode->prev != nullptr) {
         assert(currentNode->prev->next == currentNode);
      }
      currentNode = currentNode->prev;
   }
   assert(count == bCount);

   return true;
}

void DoubleLinkedList::dump() const {
   cout << "DoubleLinkedList: head=[" << head << "]  tail:[" << tail << "]" << endl;
   for(Node* currentNode = head; currentNode != nullptr; currentNode = currentNode->next) {
      cout << "   ";
      ((Cat*)currentNode)->dump();
   }
}





void DoubleLinkedList::swap(Node* node1, Node* node2) {
   assert(isIn(node1)); 
   assert(isIn(node2));
  
   Node* temp = nullptr;

   //case where nodes are indentical
   if(node1 == node2) {
      //cout << "case 1" << endl;
      return;
   }

   //all cases that swap head and tail
   if((node1 == head && node2 == tail) || (node1 == tail && node2 == head)) {
      if(node1 == head && count == 2) {
         //cout << "case 2" << endl; 
         node1->next = nullptr;
         node2->prev = nullptr;
         node1->prev = node2;
         node2->next = node1;
         head = node2;
         tail = node1;
         return;
      } else if(node2 == head && count == 2) {
         //cout << "case 3" << endl;
         node2->next = nullptr;
         node1->prev = nullptr;
         node2->prev = node1;
         node1->next = node2;
         head = node1;
         tail = node2;
         return;
      } else {
         //cout << "case 4" << endl;
         Node* oldHead = pop_front();
         Node* oldTail = pop_back();
         push_front(oldTail);
         push_back(oldHead);
         return;
      }
   }

   //all adjacent cases
   if(node1->next == node2) {
      if(node1 == head) {
         //cout << "case 5" << endl;
         node1->next = node2->next;
         node2->prev = nullptr;
         node1->prev = node2;
         node2->next = node1;
         head = node2;
         node1->next->prev = node1;
         return;
      } else if(node2 == tail) {
         //cout << "case 6" << endl;
         node2->prev = node1->prev;
         node1->next = nullptr;
         node2->next = node1;
         node1->prev = node2;
         tail = node1;
         node2->prev->next = node2;
         return;
      } else {
         //cout << "case 7" << endl;
         node1->next = node2->next;
         node2->prev = node1->prev;
         node1->prev = node2;
         node2->next = node1;
         node1->next->prev = node1;
         node2->prev->next = node2;
         return;
      }
   } else if(node2->next == node1) {
      if(node2 == head) {
         //cout << "case 8" << endl;
         node2->next = node1->next;
         node1->prev = nullptr;
         node2->prev = node1;
         node1->next = node2;
         head = node1;
         node2->next->prev = node2;
         return;
      } else if(node1 == tail) {
         //cout << "case 9" << endl;
         node1->prev = node2->prev;
         node2->next = nullptr;
         node1->next = node2;
         node2->prev = node1;
         tail = node2;
         node1->prev->next = node1;
         return;
      } else {
         //cout << "case 10" << endl;
         node2->next = node1->next;
         node1->prev = node2->prev;
         node2->prev = node1;
         node1->next = node2;
         node2->next->prev = node2;
         node1->prev->next = node1;
         return;
      }
   }
   
   //non-adjacent cases where one is the head
   if(node1 == head) {
      //cout << "case 11" << endl;
      temp = node1->next;
      node1->next = node2->next;
      node2->next = temp; 
      node1->prev = node2->prev;
      node2->prev = nullptr;
      head = node2;
      node1->prev->next = node1;
      node1->next->prev = node1;
      node2->next->prev = node2;
      return;
   } else if(node2 == head) {
      //cout << "case 12" << endl;
      temp = node2->next;
      node2->next = node1->next;
      node1->next = temp;
      node2->prev = node1->prev;
      node1->prev = nullptr;
      head = node1;
      node2->prev->next = node2;
      node2->next->prev = node2;
      node1->next->prev = node1;
      return;
   }
   
   //non-adjacent cases where one is the tail
   if(node1 == tail) {
      //cout << "case 13" << endl;
      temp = node1->prev;
      node1->prev = node2->prev;
      node2->prev = temp;
      node1->next = node2->next;
      node2->next = nullptr;
      tail = node2;
      node1->prev->next = node1;
      node1->next->prev = node1;
      node2->prev->next = node2;
      return;
   } else if(node2 == tail) {
      //cout << "case 14" << endl;
      temp = node2->prev;
      node2->prev = node1->prev;
      node1->prev = temp;
      node2->next = node1->next;
      node1->next = nullptr;
      tail = node1;
      node2->prev->next = node2;
      node2->next->prev = node2;
      node1->prev->next = node1;
      return;
   } 
   
   // the general case
   //cout << "case 15" << endl;
   temp = node1->next;
   node1->next = node2->next;
   node2->next = temp;
   temp = node1->prev;
   node1->prev = node2->prev;
   node2->prev = temp;
   node1->prev->next = node1;
   node1->next->prev = node1;
   node2->prev->next = node2;
   node2->next->prev = node2;
   
}





const bool DoubleLinkedList::isSorted() const {
   if(count <= 1) {
      return true;
   }

   for(Node* i = head; i->next != nullptr; i = i->next) {
      if(*i > *i->next) {
         return false;
      }
   }

   return true;
}

void DoubleLinkedList::insertionSort() {
   if(count <= 1) {
      return;
   }
   Node* i = head; 
   Node* next;
   Node* min;
   while(i != nullptr) {
      //dump();
      next = i->next;
      min = i;
      while(next != nullptr) {
         if(*min > *next) {
            min = next;
         }
         next = next->next;
      }
      swap(i, min);
      i = min->next;
   }
}



//Extra Credit
bool DoubleLinkedList::isIn(Node* someNode) const {
   Node* currentNode = head;

   while(currentNode != nullptr) {
      if(currentNode == someNode) {
         return true;
      }
      currentNode = currentNode->next;
   }
   return false;
}

void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode) {
   if(head == nullptr && currentNode == nullptr) {
      push_front(newNode);
      return;
   } else if (head == nullptr && currentNode != nullptr) {
      assert(false);
   } else if (head != nullptr && currentNode == nullptr) {
      assert(false);
   } else {
      assert(head != nullptr && currentNode != nullptr);
      assert(isIn(currentNode));
      assert(!isIn(newNode));
   }

   if(currentNode != tail) {
      newNode->prev = currentNode;
      newNode->next = currentNode->next;
      newNode->next->prev = newNode;
      currentNode->next = newNode;
      count++;
   } else {
      push_back(newNode);
   }

   validate();
}

void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode) {
   if(tail == nullptr && currentNode == nullptr) {
      push_back(newNode);
      return;
   } else if (tail == nullptr && currentNode != nullptr) {
      assert(false);
   } else if (tail != nullptr && currentNode == nullptr) {
      assert(false);
   } else {
      assert(tail != nullptr && currentNode != nullptr);
      assert(isIn(currentNode));
      assert(!isIn(newNode));
   }

   if(currentNode != head) {
      newNode->prev = currentNode->prev;
      newNode->next = currentNode;
      currentNode->prev->next = newNode;
      currentNode->prev = newNode;
      count++;
   } else {
      push_front(newNode);
   }

   validate();
}
